package com.grid.shedule.core;

import java.net.URL;
import java.util.Properties;

public class PropertiesReader {
	
	private static Properties PROPERTIES;
	private static String PROPERTIES_FILE="config.properties";
	
	static{
		PROPERTIES=new Properties();
		try{
		URL proper=ClassLoader.getSystemResource(PROPERTIES_FILE);
		PROPERTIES.load(proper.openStream());
		}
		catch(Exception ex)	
		{
			System.out.println(ex.toString());
		}
	}
	
	public static String getProperty(String key){
		return PROPERTIES.getProperty(key);
	}
}
