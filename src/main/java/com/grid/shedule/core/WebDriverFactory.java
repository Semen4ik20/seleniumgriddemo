package com.grid.shedule.core;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class WebDriverFactory {

	private WebDriver driver;
	private DesiredCapabilities capabilities;
	
	private DesiredCapabilities getBrowser(String browserName, String browserVersion) {
		switch (browserName) {
		case "chrome":
			capabilities = DesiredCapabilities.chrome();
			capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			capabilities.setCapability(CapabilityType.BROWSER_NAME, browserName);
			break;
		case "firefox":
			capabilities = DesiredCapabilities.firefox();
			capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			if(browserVersion != null){
				capabilities.setCapability(CapabilityType.VERSION, browserVersion);
                capabilities.setCapability(CapabilityType.BROWSER_NAME, browserName);
			}
			break;
		case "internet explorer":
			capabilities = DesiredCapabilities.internetExplorer();
			capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			if(browserVersion != null){
				capabilities.setCapability(CapabilityType.VERSION, browserVersion);
                capabilities.setCapability(CapabilityType.BROWSER_NAME, browserName);
                capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
                capabilities.setCapability(InternetExplorerDriver.INITIAL_BROWSER_URL, "about:blank");
                capabilities.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, true);
			}
			break;
		default:
			break;
		}
       return capabilities;
    }
	
	private void waitImplicitly(long timeout){
		driver.manage().timeouts().implicitlyWait(timeout, TimeUnit.SECONDS);
	}
	
	public WebDriver getWebDriver(String browserName){
		
		switch (browserName) {
		case "chrome":
			System.setProperty("webdriver.chrome.driver","GridLaunch/Drivers/chromedriver.exe");
			driver = new ChromeDriver();
			break;
		case "firefox":
			driver = new FirefoxDriver();
		default:
			break;
		}
		waitImplicitly(Integer.parseInt(PropertiesReader.getProperty("driver_delay")));
		return driver;
	}
	
	public WebDriver getWebDriver(String browserName, String browserVersion){
		
		try {
			driver = new RemoteWebDriver(new URL(PropertiesReader.getProperty("hub_address")), getBrowser(browserName, browserVersion));
			waitImplicitly(Integer.parseInt(PropertiesReader.getProperty("driver_delay")));
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return driver;
		
	}
	

}
