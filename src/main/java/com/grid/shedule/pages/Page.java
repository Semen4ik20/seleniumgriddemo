package com.grid.shedule.pages;

import com.grid.shedule.core.PropertiesReader;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.concurrent.TimeUnit;

/**
 * Created by Serhii_Pirohov on 23.06.2015.
 */
public class Page {

    WebDriver driver;

    private static final String DELAY = PropertiesReader.getProperty("driver_delay");

    public Page(WebDriver driver) {
        this.driver = driver;
        driver.manage().timeouts().implicitlyWait(Integer.valueOf(DELAY), TimeUnit.SECONDS);
    }

    protected WebElement $(By locator){
        return driver.findElement(locator);
    }

    protected void toPage(String url){
        driver.get(url);
    }

}
